require('dotenv').config();
const express = require('express'); // import
const body_parser = require('body-parser');
const app = express();
//const port = 3000; puerto fijo
const port = process.env.PORT || 3000; // puerto puede ser segun la variable o por 3000
const URL_BASE = '/apitechu/v1/';
const usersFile = require('./user.json');

app.listen(port, function(){
  console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

//operación get (collection)
app.get (URL_BASE + 'users',
      function(request,response){
        console.log ("Hola Mundo");
        console.log ("Hola Mundote");
        //response.status(200); //respuesta de codigo correcto
        response.status(200).send({usersFile});
        //response.send({usersFile}); // la respuesta (send) va al final
}) ;


// LOGIN - user.js
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("Prueba de login semana 2");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login OK");
            response.status(222).send({"msg" : "Login OK" , "idUsuario" : us.id , "logged" : "true"}); // Mandar codigo 222 prueba ok
        }
        else {
          console.log("Login NO OK");
          response.status(405).send({"msg" : "Password incorrecto."}); // Mandar error 405 solo de prueba
        }
      }
    }
});


function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   }); }


// LOGOUT - users.json
app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("Probando logout");
    var userId = request.body.id;
    for(us of usersFile) {
      if(us.id == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.status(200).send({"msg" : "Logout correcto.", "idUsuario" : us.id}); // mandar codigo error 415 solo prueba
        }
        }
        else {
          console.log("Logout incorrecto");
          response.status(415).send({"msg" : "Logout incorrecto."}); // mandar codigo error 415 solo prueba
      }
    }
});
